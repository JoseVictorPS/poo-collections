package com.aulaoo;

public class Aluno {
    private int matricula;
    private long cpf;
    private String nome;
    private String email;

    public Aluno(int mat, long c, String n, String e) {
        matricula = mat;
        cpf = c;
        nome = n;
        email = e;
    }

    public String getNome() {
        return nome;
    }

    public int getMatricula() {
        return matricula;
    }

}
