package com.aulaoo;

public class Main {

    public static void main(String[] args) {
    Turma t = new Turma();
    t.cadastraAluno(123, 1112223334, "Jose", "alou@email.com");
    t.cadastraAluno(321, 1115523389, "Paiva", "bom@email.com");
    t.cadastraAluno(444, 1112102393, "Victor", "legal@email.com");
    t.cadastraAluno(666, 1112223334, "Silva", "coe@email.com");
    System.out.println(t.buscaCpf(1112223334).getNome());
    System.out.println(t.buscaMatricula(123).getNome());
    System.out.println(t.buscaNome("Jose").getNome());
    }
}
