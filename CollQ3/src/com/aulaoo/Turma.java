package com.aulaoo;

import java.util.ArrayList;
import java.util.HashMap;

public class Turma {
    ArrayList<Aluno> alunos;
    HashMap<Long, Aluno> matriculados;

    public Turma() {
        alunos = new ArrayList<>();
        matriculados = new HashMap<>();
    }

    public void cadastraAluno(int mat, long c, String n, String e) {
        Aluno a = new Aluno(mat, c, n, e);
        alunos.add(a);
        try {
            if (buscaCpf(c) != null) {
                alunos.remove(a);
                System.out.println("Aluno já cadastrado!");
            }
        } catch (NullPointerException x) {
            matriculados.put(c, a);
            System.out.println("Aluno cadastrado com sucesso!");
        }
    }

    public Aluno buscaCpf(long c) throws NullPointerException{
        if(matriculados.get(c) == null) {
            throw new NullPointerException();
        }
        return matriculados.get(c);
    }

    public Aluno buscaNome(String n) {
        for(Aluno a : alunos){
            if(a.getNome().equals(n))return a;
        }
        return null;
    }

    public Aluno buscaMatricula(int m) {
        for(Aluno a : alunos){
            if(a.getMatricula()==m)return a;
        }
        return null;
    }

}
