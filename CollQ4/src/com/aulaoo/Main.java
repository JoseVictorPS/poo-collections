package com.aulaoo;

import java.util.HashSet;
import java.util.Random;
import java.util.ArrayList;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
	    ArrayList<Integer> l = genList();
        Set<Integer>s = new HashSet<>();
        for(Integer i : l)s.add(i);
        for(Integer g : s) System.out.println(g);
    }

    private static ArrayList genList() {
        ArrayList<Integer> l = new ArrayList<>();
        Random r = new Random();
        for(int i=0; i<40; i++)l.add(r.nextInt(10));
        return l;
    }
}
