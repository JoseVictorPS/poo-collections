package com.aulaoo;

public class Main {

    public static void main(String[] args) {
        System.out.println("Com 30mil numeros!");
        ComArray.contagem(30000);
        ComHash.contagem(30000);
        System.out.println("Com 50mil numeros!");
        ComArray.contagem(50000);
        ComHash.contagem(50000);
        /*É notado que quando o número de elementos cresce, no HashSet
        * isso não interfere muito na busca, já no ArrayList isso afeta
        * pesadamente o tempo. Na inserção o HashSet aumenta em comparação
        * a ele mesmo quando se tem menos números, enquanto o ArrayList
        * não se altera tanto*/
    }
}
